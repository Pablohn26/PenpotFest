# PenpotFest

[PenpotFest](https://penpotfest.org) is the Design event that brings designers and developers together in Barcelona (June 28-30).

This repository contains the installation links and the schedule for Events apps in `.ics` and [frab format](http://frab.github.io/frab/). 

They have been generated using [pretalx](https://pretalx.com/p/about/).

## Android

### Install the app

I recommend to install `Giggity`. You can get it from the following places:

<a href='https://play.google.com/store/apps/details?id=net.gaast.giggity'><img alt='Get it on Google Play' height="80" src='https://play.google.com/intl/es-419/badges/static/images/badges/en_badge_web_generic.png'/></a>


[<img src="https://f-droid.org/badge/get-it-on.png" alt="Get it on F-Droid" height="80">](https://f-droid.org/es/packages/net.gaast.giggity/)

### Set up the schedule 

![Instructions](./giggify-instructions.gif)

Open `Giggify` app, click on top right corner "+" and scan the following QR code:

![Schedule QR](./penpotfest-schedule-qr.jpg)

Alternatively, you can add the schedule using the following URL: 

* [https://codeberg.org/Pablohn26/PenpotFest/raw/branch/main/schedule.xml](https://codeberg.org/Pablohn26/PenpotFest/raw/branch/main/schedule.xml)

## iOS

You can download the event schedule in `.ics` format [here](https://codeberg.org/Pablohn26/PenpotFest/raw/branch/main/schedule.ics).

## Google Calendar

You can also import the event schedule to Google Calendar. To do so, [open Google Calendar settings](https://calendar.google.com/calendar/u/0/r/settings), click on `Add calendar`and then `from URL`. Introduce the following URL and click on `add calendar`.
* [https://codeberg.org/Pablohn26/PenpotFest/raw/branch/main/schedule.ics](https://codeberg.org/Pablohn26/PenpotFest/raw/branch/main/schedule.ics)